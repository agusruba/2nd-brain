One of the [[2021 goals]] and hopefully finish by [[2022 goals]] 

The process of the goal is to :

* Select superheroes for the initial batch
* Design the base case
* Modify base design for small intravenous (250ml) fluid
* Modify for (500ml)
* Modify for (1 l)
* Print 5 (500ml)
* Print 3 (250ml)
* Print 2 (1l)


December, 2021 update:

Heroes designs done:

* batman
* wonderwoman
* spiderman
* ironman
* superman

All designs are for [[two color 3d printer]]

#social_activism 