Cashflow dashboard could help to start control on expenses and to keep an eye on alternatives income-projects seeded in past years.

All the cashflow dashboards seen so far in youtube and other tutorials are examples on how to track expenses using a form so will never work for me.

This [[2022 goals]] I will try to export (CSV) all the accounts from my banks (Banamex, Banorte) and will try to categorize the transactions by descriptions... this of course will make me uncomfortable with the precision of the categorizations but improves will (possible) be done in next years.

One account will be added manually (cash) and one bank (Hey Banco) will be missed for now. This bank account is only used for savings not for expenses.

Process:

* Planning monthly expenses (with history), with the description, amount and category
* Add all the accounts for 1 month period export file
* Categorize all the outcome transactions (and add a default one)
* Planning monthly incomes with the description, amount and category
* Categorize all the income transactions (add a default one)
* Compare planned with last month transactions
* Compare all months
* Compare planned incomes with last month income transactions
* Simulate next month outcome transactions
* Simulate next year outcome transactions

This project should control my #financial_growth 

